const { initBrowser } = require("./browser");
// "https://kiwitaxi.com/agent.php/order/offer/form?order_id=1806451&driver_id=4618&act=accept&token=0c962e1c4d186a96a4a4b5848b40c841"

async function processAutomation(url) {
  const browser = await initBrowser();
  const page = await browser.newPage();

  const blocked_domains = [
    'googlesyndication.com',
    'adservice.google.com',
  ];
  
  await page.setRequestInterception(true);
  page.on('request', request => {
    const url = request.url()
    if (blocked_domains.some(domain => url.includes(domain))) {
      request.abort();
    } else {
      request.continue();
    }
  });

  await page.goto(url, {waitUntil: 'networkidle2'});
  
  var selector =
    "#app > div.app-content > div > div.v--modal-overlay.scrollable > div > div.v--modal-box.v--modal.stretched-md.stretched-xs > div > div.container > div.dialog > div > div > div";
  var formSelector =
    "#app > div.app-content > div > div.v--modal-overlay.scrollable > div > div.v--modal-box.v--modal.stretched-md.stretched-xs > div > div.footer > div > div.actions > div:nth-child(1) > a";

   await page.waitForNetworkIdle();
  if ((await page.$(selector)) !== null) {
    //  await page.waitForSelector(selector);
    const info = await page.$eval(selector, (el) => el.textContent);
    console.log(info);
    if (info != null || undefined) {
        browser.close();
    }
  } else if ((await page.$(formSelector)) !== null) {
    //  await page.waitForSelector(formSelector);
    const forminfo = await page.$eval(formSelector, (el) => el.textContent);
    console.log(forminfo);

    await page.evaluate(() => {
      for (const checkbox of document.querySelectorAll("input")) {
        if (!checkbox.checked) checkbox.click();
      }
    });

    await page.click(formSelector);
    console.log("Congragulation , Order form submited....");
  } else {
    browser.close();
  }
}


module.exports = {
  processAutomation,
};
