#!/usr/bin/env node

const fs = require("fs").promises;
const path = require("path");
const { authenticate } = require("@google-cloud/local-auth");
const { google } = require("googleapis");
const base64Url = require("base64url");
const { parse } = require("node-html-parser");

// If modifying these scopes, delete token.json.
const SCOPES = [
  "https://www.googleapis.com/auth/gmail.readonly",
  "https://www.googleapis.com/auth/gmail.modify",
];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const { processAutomation } = require("./automation");
const TOKEN_PATH = path.join(__dirname, "token.json");
const CREDENTIALS_PATH = path.join(__dirname, "credentials.json");
const express = require("express");
const cors = require("cors");
const bodyparser = require("body-parser");
const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.use(bodyparser.json({ extended: true }));
app.set("views", "views");
app.use(express.static(path.join(__dirname, "public")));

let client = null;
/**
 * Reads previously authorized credentials from the save file.
 *
 * @return {Promise<OAuth2Client|null>}
 */
async function loadSavedCredentialsIfExist() {
  try {
    const content = await fs.readFile(TOKEN_PATH);
    const credentials = JSON.parse(content);
    return google.auth.fromJSON(credentials);
  } catch (err) {
    return null;
  }
}

/**
 * Serializes credentials to a file comptible with GoogleAUth.fromJSON.
 *
 * @param {OAuth2Client} client
 * @return {Promise<void>}
 */
async function saveCredentials(client) {
  const content = await fs.readFile(CREDENTIALS_PATH);
  const keys = JSON.parse(content);
  const key = keys.installed || keys.web;
  const payload = JSON.stringify({
    type: "authorized_user",
    client_id: key.client_id,
    client_secret: key.client_secret,
    refresh_token: client.credentials.refresh_token,
  });
  await fs.writeFile(TOKEN_PATH, payload);
}

/**
 * Load or request or authorization to call APIs.
 *
 */
async function authorize() {
  client = await loadSavedCredentialsIfExist();
  if (client) {
    return client;
  }
  client = await authenticate({
    scopes: SCOPES,
    keyfilePath: CREDENTIALS_PATH,
  });
  if (client.credentials) {
    await saveCredentials(client);
  }
  return client;
}

/**
 * Lists the labels in the user's account.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */

async function listLabels(auth) {
  let orderLinks = [];

  const gmail = google.gmail({ version: "v1", auth });
  let res;
  do {
    res = await gmail.users.messages.list({
      userId: "me",
      q: "from:info@kiwitaxi.com is:unread",
      maxResults: 1,
    });
  } while (res.data.messages == undefined || null);

  if (res.data.messages != undefined || null) {
    for (const item of res.data.messages) {
      // "1849ff43b1586dca"

      const emailContent = await gmail.users.messages.get({
        userId: "me",
        id: item.id,
        format: "full",
      });

      await gmail.users.messages.modify({
        userId: "me",
        id: item.id,
        resource: {
          addLabelIds: [],
          removeLabelIds: ["UNREAD"],
        },
      });

      if (emailContent.data.payload.mimeType == "text/html") {
        var encodeBody = await emailContent.data?.payload?.body?.data;

        if (encodeBody != undefined) {
          var aTag = await parse(base64Url.decode(encodeBody), {
            comment: false,
            blockTextElements: {
              script: false,
              noscript: false,
              style: false,
            },
          }).querySelector(
            "body > table > tbody > tr:nth-child(1) > td > div > table.container.container-content > tbody > tr > td > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(1) > table:nth-child(7) > tbody > tr > td.panel > a"
          );

          var href = aTag?.getAttribute("href");

          if (href != undefined) {
            orderLinks.push(href);
          } else {
            console.log("order url not found");
          }
        } else {
          console.log("encodebody was undefined");
        }
      }
    }

    return orderLinks;
  } else {
    console.log("messages was empty");
    return orderLinks;
  }
}

async function sendReqToURLs(orderURLs) {
  for (const item of orderURLs) {
    console.log("\x1b[33m%s\x1b[0m", item);
    try {
      await processAutomation(item);
    } catch (err) {
      console.error(err);
    }
  }
}

async function run() {
  try {
    if (client == null) {
      client = await authorize();
      console.log("Authorized ");
    }

    console.log(
      "Watching INBOX to get new and unread emails from <info@kiwitaxi.com> ..."
    );
    let orderLinks = await listLabels(client);
    await sendReqToURLs(orderLinks);
    await run();
  } catch {
    await run();
  }
}
// console.log("CONNECTING...");
// run();
const router = express.Router();

router.get("/", (req, res) => {
  res.sendFile(__dirname + "/views/index.html");
});

router.get("/startbot", async (req, res) => {
  res.send("Bot is running successfuly , orders are watching to get ...");
  await run();
});

app.use(router);

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
